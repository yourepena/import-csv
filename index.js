const csv = require('csv-parser')
const fs = require('fs')
var mysql = require('mysql');
var connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'gpolitica'
});
connection.connect();
fs.createReadStream('data.csv')
  .pipe(csv({ separator: ';' }))
  .on('data', (data) => {
    var uf = null;
    var codMunicipio = null;
    var Eleitorado2016 = null;
    var PercentualVotosObtidosPsc = 0;
    var TotalVotosNominais = null;
    var TotalVotosLegenda = null;
    var TotalVotos = null;
    var VotosNominaisPsc = null;
    var votosLegendaPsc = null;
    var TotalVotosPsc = null;
    var qtdeCargoVereadores = null;
    var quocienteEleitoral2016 = null;
    if (data.uf.trim())
      uf = data.uf;
    if (data.codMunicipio.trim())
      codMunicipio = data.codMunicipio;
    if (data.Eleitorado2016.trim())
      Eleitorado2016 = data.Eleitorado2016;
    if (data.PercentualVotosObtidosPsc.trim())
      PercentualVotosObtidosPsc = data.PercentualVotosObtidosPsc;
    if (data.TotalVotosNominais.trim())
      TotalVotosNominais = data.TotalVotosNominais;
    if (data.TotalVotosLegenda.trim())
      TotalVotosLegenda = data.TotalVotosLegenda;
    if (data.TotalVotos.trim())
      TotalVotos = data.TotalVotos;
    if (data.VotosNominaisPsc.trim())
      VotosNominaisPsc = data.VotosNominaisPsc;
    if (data.votosLegendaPsc.trim())
      votosLegendaPsc = data.votosLegendaPsc;
    if (data.TotalVotosPsc.trim())
      TotalVotosPsc = data.TotalVotosPsc;
    if (data.qtdeCargoVereadores.trim())
      qtdeCargoVereadores = data.qtdeCargoVereadores;
    if (data.quocienteEleitoral2016.trim())
      quocienteEleitoral2016 = data.quocienteEleitoral2016;
    var query = `INSERT INTO tbl_dados_eleitorais`
      + `(ano,uf,id_munic,eleitorado,perc_eleitorado,qt_votos,qt_leg,qt_validos,votos_validos_psc,votos_legenda_psc,total_votos_psc,id_cargo_pol,qtd,quoeficiente,tipo)`
      + `VALUES (2016, '${uf}',${codMunicipio},${Eleitorado2016},'${PercentualVotosObtidosPsc}',${TotalVotosNominais},${TotalVotosLegenda},${TotalVotos},${VotosNominaisPsc},${votosLegendaPsc},${TotalVotosPsc},11,${qtdeCargoVereadores},${quocienteEleitoral2016},1);`;
    console.log(query);
    connection.query(query);
  })
  .on('end', () => {
    //console.log("Terminou de importar");
  });

